<?php
return [
    #
    'worker'     => 'worker/indexPage',
    'worker/add' => 'worker/addPage',

    ''              => 'film/viewAllPage',
    'films'         => 'film/viewAllPage',
    'film/([0-9]+)' => 'film/view/$1',

    'newpage' => 'film/testPage',

    'ajax/booking/([0-9]+)' => 'user/ajaxBooking/$1',
];