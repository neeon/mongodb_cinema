-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 11, 2018 at 06:42 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinema2`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `continuance_min` int(11) NOT NULL,
  `age_min` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `name`, `continuance_min`, `age_min`) VALUES
(1, 'Сплит', 117, 17),
(3, 'Гадкий я 3', 89, 6),
(4, 'Мумия', 110, 16),
(5, 'Четверо против банка', 92, 16),
(6, 'Салют-7', 106, 12),
(7, 'Уличные танцы 2', 84, 12),
(8, 'Механик', 92, 16),
(9, 'Без компромиссов', 97, 18),
(10, 'Миф', 120, 18);

-- --------------------------------------------------------

--
-- Table structure for table `film_genre`
--

CREATE TABLE `film_genre` (
  `id` int(11) NOT NULL,
  `film_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `film_genre`
--

INSERT INTO `film_genre` (`id`, `film_id`, `genre_id`) VALUES
(1, 1, 3),
(3, 1, 4),
(4, 1, 5),
(8, 3, 1),
(6, 4, 1),
(7, 4, 3),
(5, 4, 6),
(9, 6, 8),
(11, 7, 3),
(10, 7, 10),
(12, 7, 11),
(14, 8, 3),
(13, 8, 6),
(15, 9, 3),
(16, 9, 4),
(17, 10, 6),
(18, 10, 7);

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `name`) VALUES
(6, 'Боевики'),
(8, 'Драмы'),
(3, 'Зарубежные'),
(1, 'Комедии'),
(12, 'Криминал'),
(11, 'Мелодрамы'),
(10, 'Мюзиклы'),
(2, 'Приключения'),
(4, 'Триллеры'),
(5, 'Ужасы'),
(7, 'Фантастика');

-- --------------------------------------------------------

--
-- Table structure for table `hall`
--

CREATE TABLE `hall` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '1',
  `maxCountPlace` int(11) NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hall`
--

INSERT INTO `hall` (`id`, `number`, `maxCountPlace`) VALUES
(1, 1, 60),
(2, 2, 50);

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `film_id` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `film_id`, `hall_id`, `price`, `date`) VALUES
(1, 4, 2, 120, '2018-07-05 13:00:00.000000'),
(2, 5, 2, 185, '2018-07-05 13:50:00.000000'),
(3, 7, 2, 185, '2018-07-06 14:00:00.000000'),
(4, 8, 1, 185, '2018-07-08 12:00:00.000000'),
(5, 10, 1, 185, '2018-07-09 11:00:00.000000'),
(6, 4, 2, 350, '2018-07-09 12:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `place` int(11) NOT NULL,
  `date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`id`, `session_id`, `user_id`, `place`, `date`) VALUES
(1, 1, 1, 50, '2018-07-18 00:00:00.000002'),
(3, 1, 1, 48, '2018-07-05 00:00:00.000002'),
(5, 3, 1, 50, '2018-07-18 00:00:00.000002');

--
-- Triggers `ticket`
--
DELIMITER $$
CREATE TRIGGER `ticket_CHECK-PLACE_BEFORE_INSERT` BEFORE INSERT ON `ticket` FOR EACH ROW BEGIN
/*
    * Ограничение на выбор места в зале при бронировании билета.
    * Запрещено выбирать больше максимального значения.
    *
    * В противном случае значение не будет изменено.
    */
	
	SET @session_id 		= NEW.`session_id`;
    SET @hall_id 			= (SELECT `hall_id` FROM `session` WHERE `id` = @session_id);
    SET @maxCountPlaceHall	= (SELECT `maxCountPlace` FROM `hall` WHERE `id` = @hall_id); 
    
    IF NEW.`place` > @maxCountPlaceHall THEN
		SIGNAL SQLSTATE '45000' set message_text = 'The chosen place is more than the maximum for this hall.';
	END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ticket_CHECK-PLACE_BEFORE_UPDATE` BEFORE UPDATE ON `ticket` FOR EACH ROW BEGIN
	/*
    * Ограничение на выбор места в зале при бронировании билета.
    * Запрещено выбирать больше максимального значения.
    *
    * В противном случае значение не будет изменено.
    */
	
	SET @session_id 		= OLD.`session_id`;
    SET @hall_id 			= (SELECT `hall_id` FROM `session` WHERE `id` = @session_id);
    SET @maxCountPlaceHall	= (SELECT `maxCountPlace` FROM `hall` WHERE `id` = @hall_id); 
    
    IF NEW.`place` > @maxCountPlaceHall THEN
		SIGNAL SQLSTATE '45000' set message_text = 'The chosen place is more than the maximum for this hall.';
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `full_name` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `password` varchar(65) NOT NULL,
  `dateOfReg` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `full_name`, `phone`, `password`, `dateOfReg`) VALUES
(1, 'asd2', '123123', 'dasы', '2018-07-06 00:00:00.000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_age_min` (`id`);

--
-- Indexes for table `film_genre`
--
ALTER TABLE `film_genre`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_film_genre` (`film_id`,`genre_id`),
  ADD KEY `fk_fg_film_id_idx` (`film_id`),
  ADD KEY `fk_fg_genre_id_idx` (`genre_id`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `hall`
--
ALTER TABLE `hall`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `number_UNIQUE` (`number`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ssn_film_id_idx` (`film_id`),
  ADD KEY `fk_ssn_hall_id_idx` (`hall_id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_session_place` (`session_id`,`place`),
  ADD KEY `fk_tckt_session_id_idx` (`session_id`),
  ADD KEY `fk_ticket_user_id_idx` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_UNIQUE` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `film_genre`
--
ALTER TABLE `film_genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `hall`
--
ALTER TABLE `hall`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `film_genre`
--
ALTER TABLE `film_genre`
  ADD CONSTRAINT `fk_fg_film_id` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fg_genre_id` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `fk_ssn_film_id` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ssn_hall_id` FOREIGN KEY (`hall_id`) REFERENCES `hall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `fk_tckt_session_id` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ticket_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
