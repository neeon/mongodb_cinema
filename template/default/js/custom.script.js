// Выбор места при бронировании
$selectPlaces = [];

$('.page-session-view table button').on('click', function (e) {
    $place = parseInt($(this).text());

    if ($(this).attr('data-select') == undefined) {
        if ($selectPlaces.indexOf($place) == -1) {
            $(this).removeClass('btn-primary').addClass('btn-success');
            $selectPlaces.push($place);
        } else {
            $selectPlaces.remove($place);
            $(this).removeClass('btn-success').addClass('btn-primary');
        }

        $countPlaces = $selectPlaces.length;
        $priceTotal = $countPlaces * session_price;
        $('[data-price-value=""]').html($priceTotal);

    } else {
        alert('Это место уже забронировано.');
    }
});

// Бронирование места
$('[data-action="booking"]').on('click', function (e) {
    if ($selectPlaces.length > 0) {
        $.post('/ajax/booking/' + session_id, {selectPlacesArr: $selectPlaces}, onAjaxSuccess);

        function onAjaxSuccess(data) {
            data = JSON.parse(data);

            if (data['result'] === true) {
                $('#success-message').fadeIn('slow');
                $('#booking-session .card').remove();
            } else {
                alert(data['message']);
            }
        }
    } else {
        alert('Вы ничего не выбрали!');
    }
});


// Удаление элемента из массива
Array.prototype.remove = function (value) {
    var idx = this.indexOf(value);
    if (idx != -1) {
        return this.splice(idx, 1);
    }
    return false;
}