<?php

namespace App\Models;


class Film
{
    /**
     * Вернет массиив с информацией о жанрах для фильма с указаным Id.
     *
     * @param $filmId
     *
     * @return array
     */
    public static function getGenreByFilmId($filmId)
    {
        $db = \App\Components\MySQL::getConnection();

        $stmp = $db->prepare('SELECT
                                        genre_id,
                                        genre.name
                                        
                                        FROM
                                        film_genre
                                        
                                        INNER JOIN genre
                                        ON film_genre.genre_id = genre.id
                                        
                                        WHERE
                                        film_id = :filmId');
        $stmp->execute(['filmId' => $filmId]);
        $stmp->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmp->fetchAll();

        return $result;
    }

    /**
     * Получить список фильмов для активных сеансов.
     *
     * @return array
     */
    public static function getFilmsActiveSessions()
    {
        $db   = \App\Components\MySQL::getConnection();
        $stmp = $db->prepare('SELECT
                                        film.name as film_name,
                                        film.id as film_id,
                                        film.age_min,
                                        film.continuance_min                                        
                                        FROM 
                                        `session`
                                        
                                        INNER JOIN film
                                        ON `session`.film_id = film.id
                                                                               
                                        LEFT JOIN hall
                                        ON `session`.hall_id = hall.id
                                        
                                        
                                        GROUP BY `session`.film_id;
                                       ');

        $stmp->execute([]);
        $stmp->setFetchMode(\PDO::FETCH_ASSOC);
        $sessions = $stmp->fetchAll();

        # Получить информацию о жанрах для каждого фильма
        for ($i = 0; $i < count($sessions); $i++) {
            # Получить все жанры к которым относиться фильм
            $genresFilm
                = \App\Models\Film::getGenreByFilmId($sessions[$i]['film_id']);

            $sessions[$i]['genres']
                = \App\Models\Genre::arrGenresToStringGenres($genresFilm);
        }

        return $sessions;
    }


    /**
     * Получить основную информацию о фильме (название, длительность, возрастные ограничение)
     * с указанным Id
     *
     * @param $filmId
     *
     * @return array
     */
    public static function getFilmById($filmId) {
        $db   = \App\Components\MySQL::getConnection();
        $stmp = $db->prepare('SELECT 
                                       *
                                       FROM
                                       film
                                       WHERE
                                       id = :filmId');
        $stmp->execute(['filmId' => $filmId]);
        $stmp->setFetchMode(\PDO::FETCH_ASSOC);

        $film = $stmp->fetch();
        $film['genres'] = self::getGenreByFilmId($filmId);
        $film['genres'] = \App\Models\Genre::arrGenresToStringGenres($film['genres']);

        return $film;
    }
}