<?php

namespace App\Models;


class User
{
    /**
     * Авторизируем пользователя
     */
    public static function auth($userId)
    {
        $_SESSION['user']['id'] = $userId;
    }

    /**
     * Проверяет авторизирован ли пользователь.
     * Вернет ID пользователя или перенаправит
     * на страницу входа
     */
    public static function checkLogged()
    {
        $userId = $_SESSION['user']['id'];
        if ($userId > 0) {
            return $userId;
        } else {
            header('Location: /login');
        }
    }

    /**
     * Проверяет является ли пользователь гостем
     */
    public static function isGuest()
    {
        $result = true;

        if (isset($_SESSION['user']['id'])) {
            if ($_SESSION['user']['id'] > 0) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Выходим с профиля пользователя
     */
    public static function logout()
    {
        unset($_SESSION['user']);
        header('Location: /login');
    }

    /**
     * Вернет массив с информацией о пользователе
     */
    public static function getUserById($userId)
    {
        $db = \App\Components\MySQL::getConnection();
        $sql
            = $db->prepare('SELECT id, full_name, dateOfReg, phone 
                                      FROM `user` WHERE id = :id');
        $sql->execute(['id' => $userId]);
        $result = $sql->fetch(\PDO::FETCH_ASSOC);

        return $result;
    }
}