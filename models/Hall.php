<?php

namespace App\Models;


class Hall
{
    # Количество мест в ряду
    const COUNT_PLACES_IN_ROW = 10;

    /**
     * Получает максимальное количество мест в зале с указаным Id.
     *
     * @param $hallId
     *
     * @return int
     */
    public static function getMaxCountPlacesByHallId($hallId)
    {
        $db   = \App\Components\MySQL::getConnection();
        $stmp = $db->prepare('SELECT
                                       maxCountPlace
                                       FROM
                                       hall
                                       WHERE
                                       id = :hallId');

        $stmp->execute(['hallId' => $hallId]);
        $stmp->setFetchMode(\PDO::FETCH_ASSOC);

        $maxCountPlace = $stmp->fetch();
        $maxCountPlace = $maxCountPlace['maxCountPlace'];

        return (int)$maxCountPlace;
    }

    /**
     * Получает количество рядов в зале.
     * Максимальное количество мест / количество мест в ряду
     *
     * @return int - количество рядов в зале (целое число).
     */
    public static function getCountRowInHall($maxCountPlace)
    {
        $row = $maxCountPlace / self::COUNT_PLACES_IN_ROW;
        $row = ceil($row);

        return $row;
    }

    /**
     * Формирует массив, который содержит в себе количество мест
     * для каждого ряда в зале.
     *
     * @param $maxCountPlace
     *
     * @return array
     */
    public static function getCountPlacesForAllRows($maxCountPlace)
    {
        $places = [];

        # Количество рядов в зале
        $row = self::getCountRowInHall($maxCountPlace);

        # К-ство мест которые остались (для формулы)
        $havePlaces = $maxCountPlace + self::COUNT_PLACES_IN_ROW;

        $i = 0;

        while ($havePlaces > self::COUNT_PLACES_IN_ROW) {
            $havePlaces -= self::COUNT_PLACES_IN_ROW;

            if ($havePlaces >= self::COUNT_PLACES_IN_ROW) {
                $places[$i] = self::COUNT_PLACES_IN_ROW;
            } else {
                $places[$i] = $havePlaces;
            }

            $i++;
        }

        return $places;
    }
}