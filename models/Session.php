<?php

namespace App\Models;

use App\Components\MySQL;

class Session
{


    /**
     * Получает информацию о всех сеансах с указанным Id
     *
     * @param $filmId
     *
     * @return array $sessions
     */
    public static function getSessionsByFilmId($filmId)
    {
        $db   = \App\Components\MySQL::getConnection();
        $stmp = $db->prepare('SELECT
                                        `session`.id as session_id,
                                        DATE_FORMAT(`date`, "%d.%m.%Y, %H:%i") as `date`,
                                        price,
                                        hall.number as hall_number,
                                        hall.id as hall_id,
                                        hall.maxCountPlace as hall_max_count_places
                                        
                                        FROM 
                                        `session`
                                        
                                        LEFT JOIN hall
                                        ON `session`.hall_id = hall.id
                                        
                                        WHERE 
                                        `session`.film_id = :filmId');
        $stmp->execute(['filmId' => $filmId]);
        $stmp->setFetchMode(\PDO::FETCH_ASSOC);

        $sessions = $stmp->fetchAll();

        for ($i = 0; $i < count($sessions); $i++) {
            $sessions[$i]['countFreePlaces']
                = self::getCountFreePlaceBySessionId($sessions[$i]['session_id']);
        }

        return $sessions;
    }


    /**
     * Получить количество свободных мест для сеанса с указанным Id.
     *
     * @param $sessionId
     *
     * @return int
     */
    public static function getCountFreePlaceBySessionId($sessionId)
    {
        $db   = \App\Components\MySQL::getConnection();
        $stmp = $db->prepare('SELECT
                                        hall.maxCountPlace,
                                        COUNT(ticket.session_id) as `countBookedPlaces`
                                        
                                        FROM 
                                        `session`
                                        
                                        LEFT JOIN hall
                                        ON `session`.hall_id = hall.id
                                        
                                        LEFT JOIN ticket
                                        ON `session`.id = ticket.session_id
                                        
                                        WHERE 
                                        `session`.id = :sessionId
                                       ');

        $stmp->execute(['sessionId' => $sessionId]);
        $stmp->setFetchMode(\PDO::FETCH_ASSOC);

        $result          = $stmp->fetch();
        $countFreePlaces = $result['maxCountPlace']
                           - $result['countBookedPlaces'];

        return (int)$countFreePlaces;
    }

    /**
     * Получает основную информацию о сеансе
     * (id_film, id_hall, price, date) с заданным Id.
     *
     * @param $sessionId
     *
     * @return array
     */
    public static function getSessionById($sessionId)
    {
        $db   = \App\Components\MySQL::getConnection();
        $stmp = $db->prepare('SELECT
                                        *
                                        FROM
                                        `session`
                                        WHERE
                                        id = :sessionId');

        $stmp->execute(['sessionId' => $sessionId]);
        $stmp->setFetchMode(\PDO::FETCH_ASSOC);

        $session = $stmp->fetch();

        return $session;
    }

    /**
     * Получает список забронированных место для сеанса с указанным Id.
     *
     * @param $sessionId
     *
     * @return array
     */
    public static function getBookedPlacesBySessionId($sessionId)
    {
        $db   = \App\Components\MySQL::getConnection();
        $stmp = $db->prepare('SELECT
                                        place
                                        FROM 
                                        ticket
                                        WHERE
                                        session_id = :sessionId');

        $stmp->execute(['sessionId' => $sessionId]);
        $stmp->setFetchMode(\PDO::FETCH_ASSOC);

        $bookedPlacesResult = $stmp->fetchAll();
        $bookedPlaces       = [];

        for ($i = 0; $i < count($bookedPlacesResult); $i++) {
            $place                = $bookedPlacesResult[$i]['place'];
            $bookedPlaces[$place] = true;
        }

        unset($bookedPlacesResult);

        return $bookedPlaces;
    }

    /**
     * Проверить все ли места в массиве являются свободными для
     * указаного сеанса
     *
     * @param array $places
     * @param       $sessionId
     *
     * @return bool
     * @throws \Exception
     */
    public static function isFreePlaces(array $places, $sessionId)
    {
        $bookedPlaces = self::getBookedPlacesBySessionId($sessionId);

        foreach ($bookedPlaces as $placeNumber => $boolean) {
            if (in_array($placeNumber, $places)) {
                throw new \Exception('Попытка повторного бронирования места: '
                                     . $placeNumber . ' (СеансID: ' . $sessionId
                                     . ')');
            }
        }

        return true;
    }
}