<?php
/**
 * Created by PhpStorm.
 * User: student-4
 * Date: 7/11/18
 * Time: 5:56 PM
 */

namespace App\Models;


class Genre
{
    /**
     * Верент строку из названиями всех жанров переданых в $arrGenres
     *
     * @param array $arrGenres - жанры. Структура: [i] -> [genre_id],[name]
     *
     * @return string
     */
    public static function arrGenresToStringGenres(array $arrGenres)
    {
        $str = '';

        foreach ($arrGenres as $genre) {
            $str .= $genre['name'] . ', ';
        }

        if (count($arrGenres) > 0) {
            $str = substr($str, 0, -2);
        }

        return (string)$str;
    }
}