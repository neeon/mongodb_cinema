<?php

namespace App\Models;


class Ticket
{
    /**
     * Добавляет запись в базу данных о бронировании места.
     *
     * @param $places
     * @param $sessionId
     * @param $userId
     */
    public static function add($places, $sessionId, $userId)
    {
        $db = \App\Components\MySQL::getConnection();

        foreach ($places as $place) {
            $stmp
                = $db->prepare('INSERT INTO ticket 
                                          VALUES (null, :sessionId, :userId, :place, :date)');
            $stmp->execute([
                'sessionId' => $sessionId,
                'userId'    => $userId,
                'place'     => $place,
                'date'      => date('Y-m-d h:i:s', time())
            ]);
        }
    }
}