<?php

namespace App\Models;

class Worker
{
    /**
     * Возвращает массив в котором содержиться информация о сотрудниках.
     *
     * @return  array
     */
    public static function getWorkers()
    {
        $mgDb       = \App\Components\MongoDb::getConnection();
        $collection = $mgDb->selectCollection('worker');
        $workersArr = $collection->find()->toArray();

        $worker = [];

        for ($i = 0; $i < count($workersArr); $i++) {
            $worker[$i]['id']       = $workersArr[$i]['_id'];
            $worker[$i]['name']     = $workersArr[$i]['name'];
            $worker[$i]['age']      = $workersArr[$i]['age'];
            $worker[$i]['phone']    = $workersArr[$i]['phone'];
            $worker[$i]['address']  = $workersArr[$i]['address'];
            $worker[$i]['position'] = $workersArr[$i]['position'];
        }

        return $worker;
    }

    /**
     * Удалить сотрудника с заданным Id.
     *
     * @param $workerId
     *
     * @return true|\Exception
     */
    public static function removeWorkerById($workerId)
    {
        $mgDb       = \App\Components\MongoDb::getConnection();
        $collection = $mgDb->selectCollection('worker');

        $workerId = \App\Components\MongoDb::convertId($workerId);

        try {
            $collection->deleteOne(['_id' => $workerId]);

            return true;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Проверяет корректность данных при добавлении нового сотрудника.
     *
     * @param $name
     * @param $age
     * @param $phone
     * @param $address
     * @param $position
     *
     * @return true|bool
     */
    public static function checkDataBeforeAdd(
        $name,
        $age,
        $phone,
        $address,
        $position
    ) {
        $error = [];

        if ( ! self::checkStrLength($name, [3, 30])) {
            $error[]
                = 'Некоректно задано имя! Мин. длинна должна составлять 3 символа, макс. 30.';
        }
        if ( ! self::checkAge($age, [10, 50])) {
            $error[] = 'Некоректно указан возраст.';
        }
        if ( ! self::checkPhone($phone)) {
            $error[]
                = 'Некоректно указан контактный номер. Необходимый формат: +380ххххххххх';
        }
        if ( ! self::checkStrLength($address, [7, 50])) {
            $error[]
                = 'Некоректно указан адресс! Мин. длинна должна составлять 7 символа, макс. 50.';
        }
        if ( ! self::checkStrLength($position, [3, 15])) {
            $error[]
                = 'Некоректно указана позиция! Мин. длинна должна составлять 3 символа, макс. 15.';
        }

        if (count($error) > 0) {
            return $error;
        }

        return true;
    }

    /**
     * Проверяет строку на наличие заданого диапазона количества символов.
     *
     * @param $str
     * @param $rangeLength - содержит два значения: мин. и макс. количество сисволов
     *
     * @return bool
     */
    private static function checkStrLength($str, $rangeLength)
    {
        $minLength = $rangeLength[0];
        $maxLength = $rangeLength[1];
        $strLength = mb_strlen($str, 'UTF-8');

        if ($strLength >= $minLength && $strLength <= $maxLength) {
            return true;
        }

        return false;
    }

    /**
     * Проверяет возраст работника.
     * Значение $age должно входить в диапазон возможного
     * возраста $rangeAge.
     *
     * @param       $age
     * @param array $rangeAge - содержит два значения: мин. и макс. возраст
     *
     * @return bool
     */
    private static function checkAge($age, array $rangeAge)
    {
        $minAge = $rangeAge[0];
        $maxAge = $rangeAge[1];

        if ($age >= $minAge && $age <= $maxAge) {
            return true;
        }

        return false;
    }

    /**
     * Проверяет коректность контактного телефона.
     *
     * @param $phone
     *
     * @return bool
     */
    private static function checkPhone($phone)
    {
        if (preg_match('~^[+]380[0-9]{9}$~', $phone)) {
            return true;
        }

        return false;
    }

    /**
     * Добвить пользователя в базу данных.
     *
     * @param $name
     * @param $age
     * @param $phone
     * @param $address
     * @param $position
     *
     * @return bool
     */
    public static function add($name, $age, $phone, $address, $position)
    {
        $mgDb       = \App\Components\MongoDb::getConnection();
        $collection = $mgDb->selectCollection('worker');

        $result = $collection->insertOne([
            'name'     => $name,
            'age'      => $age,
            'phone'    => $phone,
            'address'  => $address,
            'position' => $position,
        ]);

        if($result) {
            return true;
        }

        return false;
    }
}