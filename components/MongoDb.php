<?php
namespace App\Components;

use \MongoDB as Mongo_db;

class MongoDb
{
    /**
     * Возвращает объект для работы с MongoDb.
     *
     * @return \MongoDB\Client
     */
    static public function getConnection()
    {
        $params = require ROOT . '/config/mongodb.params.php';

        $uri = 'mongodb://' . $params['host'] . ':' . $params['port'];

        try {
            $client = (new Mongo_db\Client($uri));
            $client = $client->selectDatabase($params['db']);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die();
        }

        return $client;
    }

    /**
     * Преобразовывает Id относительно необходимого формата.
     * Если Id является строковым ключом - функция вернет Object("<id>"),
     * в ином случае вернется цифровое значение Id.
     *
     * @param $id
     *
     * @return int|string
     */
    static public function convertId($id) {
        if(strlen($id) == 24) {
            $id = new \MongoDB\BSON\ObjectId($id);
        } else {
            $id = (int) $id;
        }

        return $id;
    }
}