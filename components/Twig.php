<?php

namespace App\Components;

class Twig
{
    /**
     * Подставляет передаваемые данные в указанный шаблон Twig
     * и выводит результат.
     *
     * @param       $fileName
     * @param array $dataArr
     *
     * @throws \Exception
     */
    public static function getFile($fileName, array $dataArr)
    {
        $patternsDir = ROOT . '/views';

        $loader = new \Twig_Loader_Filesystem($patternsDir);
        $twig   = new \Twig_Environment($loader);

        if (file_exists($patternsDir . '/' . $fileName)) {
            echo $twig->render($fileName, $dataArr);
        } else {
            throw new \Exception('Шаблон не найден.');
        }
    }
}