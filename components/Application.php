<?php

namespace App\Components;


class Application
{
    public static $isGuest = true;

    public function __construct()
    {
        if ( ! \App\Models\User::isGuest()) {
            self::$isGuest = false;
        }
    }
}