<?php

namespace App\Components;

class MySQL
{
    /**
     * Возвращает объект для работы с MySQL.
     *
     * @return \PDO
     */
    static public function getConnection()
    {
        $paramsPath = ROOT . '/config/mysql.params.php';
        $params = require $paramsPath;

        try {
            $dsn = 'mysql:host=' . $params['host'] . ';dbname=' . $params['db'] . ';charset=' . $params['charset'];

            $pdo = new \PDO($dsn, $params['UserController'], $params['pass']);
        } catch (\PDOException $e) {
            die('Подключение не удалось: ' . $e->getMessage());
        }

        return $pdo;
    }

}