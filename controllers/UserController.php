<?php

namespace App\Controllers;

use App\Components\Application;
use App\Components\Twig;

class UserController extends Application
{

    /**
     * Бронирование сеанса.
     *
     * @param $sessionId
     */
    public function actionAjaxBooking($sessionId)
    {
        # Id пользователя
        $userId = \App\Models\User::checkLogged();

        # Массив с выбраными местами
        $selectPlacesArr = $_POST['selectPlacesArr'];

        try {
            $isFreePlaces = \App\Models\Session::isFreePlaces($selectPlacesArr,
                $sessionId);
            \App\Models\Ticket::add($selectPlacesArr, $sessionId, $userId);

            $result = ['result' => true];
        } catch (\Exception $e) {
            $result = ['result' => false, 'message' => $e->getMessage()];
        }

        # JSON ответ
        $result = json_encode($result);

        echo $result;
    }
}