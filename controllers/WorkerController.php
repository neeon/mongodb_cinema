<?php

namespace App\Controllers;

use App\Models\Worker;
use App\Components\Application;
use App\Components\Twig;

class WorkerController extends Application
{
    /**
     * Страница "Список пользователей".
     */
    public function actionIndexPage()
    {
        # Удалить сотрудника
        if (isset($_POST['remove_worker'])) {
            $workerId = $_POST['id_worker'];

            $isRemove = \App\Models\Worker::removeWorkerById($workerId);
        }

        # Список сотрудников
        $workersArr = \App\Models\Worker::getWorkers();

        # Отображение страницы
        try {
            $dataPage = ['workersArr' => $workersArr];

            if (!empty($isRemove)) {
                $buff = [
                    'workerId' => $workerId,
                    'isRemove' => $isRemove
                ];

                $dataPage = array_merge($dataPage, $buff);
                unset($buff);
            }

            Twig::getFile('worker/index.twig', $dataPage);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }


    /**
     * Страница "Добавить работника".
     */
    public function actionAddPage()
    {
        if (isset($_POST['add_worker'])) {
            $workerData = [
                'name'     => $_POST['name'],
                'age'      => $_POST['age'],
                'phone'    => $_POST['phone'],
                'address'  => $_POST['address'],
                'position' => $_POST['position'],
            ];

            # Проверить корректность данных
            $isCorrect
                = \App\Models\Worker::checkDataBeforeAdd(
                $workerData['name'],
                $workerData['age'], $workerData['phone'],
                $workerData['address'], $workerData['position']
            );

            # Добавить сотрудника
            if ($isCorrect === true) {
                \App\Models\Worker::Add(
                    $workerData['name'],
                    $workerData['age'], $workerData['phone'],
                    $workerData['address'], $workerData['position']
                );

                # Очистить данные пользователя
                unset($workerData);
                unset($_POST);
            }
        }

        # Отображение страницы
        try {
            $dataPage = [];

            if (!empty($isCorrect)) {
                $dataPage = ['isCorrect'  => $isCorrect];
            }

            if(!empty($workerData)) {
                $dataPage = array_merge($dataPage, ['workerData' => $workerData]);
            }

            Twig::getFile('worker/add.twig', $dataPage);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}