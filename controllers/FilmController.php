<?php

namespace App\Controllers;

use App\Components\Application;
use App\Components\Twig;

class FilmController extends Application
{
    /**
     * Страница "Список фильмов активных сеансов"
     */
    public function actionViewAllPage()
    {
        # Получить список сеансов
        $sessionsArr = \App\Models\Film::getFilmsActiveSessions();

        # Отображение страницы
        try {
            Twig::getFile('film/index.twig', ['sessions' => $sessionsArr]);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Страница "Просмотр фильма"
     *
     * @param $sessionId
     */
    public function actionView($filmId)
    {
        # Показывать таблицу бронирования
        $isShowBooking = false;

        # Получить все сеансы фильма
        $sessionsOfFilm = \App\Models\Session::getSessionsByFilmId($filmId);

        # Получить информацию о фильме
        $film = \App\Models\Film::getFilmById($filmId);

        # Показать таблицу бронирования
        if (isset($_POST['show_booking'])) {
            $isShowBooking = true;
            $sessionId     = (int)$_POST['session_id'];

            # Мест в ряду
            $countPlacesInRow = \App\Models\Hall::COUNT_PLACES_IN_ROW;

            # Получить информацию о сеансе
            $sessionIndex = array_search($sessionId,
                array_column($sessionsOfFilm, 'session_id'));
            $session      = $sessionsOfFilm[$sessionIndex];
            unset($sessionIndex);

            # Максимальное количество мест в зале
            $maxCountPlaceInHall = $session['hall_max_count_places'];


            # Количество рядов в зале
            $countRowInHall
                = \App\Models\Hall::getCountRowInHall($maxCountPlaceInHall);

            # Количество мест для каждого ряда
            $placesInRowsForAllRows
                = \App\Models\Hall::getCountPlacesForAllRows($maxCountPlaceInHall);

            # Список забронированых мест
            $bookedPlaces
                = \App\Models\Session::getBookedPlacesBySessionId($sessionId);
        }

        # Отображение страницы
        try {
            $dataPage = [
                'isShowBooking'  => $isShowBooking,
                'sessionsOfFilm' => $sessionsOfFilm,
                'film'           => $film,
            ];

            if ($isShowBooking) {
                $buff = [
                    'sessionId'              => $sessionId,
                    'countPlacesInRow'       => $countPlacesInRow,
                    'session'                => $session,
                    'maxCountPlaceInHall'    => $maxCountPlaceInHall,
                    'countRowInHall'         => $countRowInHall,
                    'placesInRowsForAllRows' => $placesInRowsForAllRows,
                    'bookedPlaces'           => $bookedPlaces,
                ];

                $dataPage = array_merge($dataPage, $buff);
            }

            Twig::getFile('film/view.twig', $dataPage);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}